segment "text"

global my_strlen
my_strlen:
	enter 0,0
	push ebx
	mov ebx, [ebp + 8]

	mov eax, 0
	
strlen_looper:
	cmp byte [ebx + eax], 0
	je strlen_leave
	inc eax
	jmp strlen_looper

strlen_leave:
	pop ebx
	leave
	ret
	
global my_memcpy
my_memcpy:
	enter 0,0
	push edi
	push esi
	
	mov edi, [ebp + 8]
	mov esi, [ebp + 12]
	mov ecx, [ebp + 16]
	
	rep movsb

	pop esi
	pop edi
	leave
	ret
