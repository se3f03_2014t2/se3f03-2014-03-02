all: main1 main2

CFLAGS=-m32 -O2
LDFLAGS=-m32

main2:	main2.o	asm.o

asm.o:	asm.asm
	nasm -f elf -o asm.o asm.asm
