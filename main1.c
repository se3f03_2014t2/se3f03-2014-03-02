#include <stdio.h>

int my_strlen(const char *chars) {
	int i = 0;
	while(chars[i] != '\0') {
		i++;
	}
	return i;
}

void my_memcpy(char *dest, const char *src, int n) {
	int i;
	for(i = 0; i < n; ++i) {
		dest[i] = src[i];
	}
}

int main() {
	const char *in = "Hello World!";
	int len = my_strlen(in);
	
	char myarray[50];
	my_memcpy(myarray, in, len);
	myarray[len] = 0;
	printf("Copied %d chars, of: %s.  Started: %s\n", len, myarray, in);
}
